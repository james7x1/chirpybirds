# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/allen/workarea/gnuradio/gr-juha/build/swig/_juha_swig_doc_tag.cpp" "/home/allen/workarea/gnuradio/gr-juha/build/swig/CMakeFiles/_juha_swig_doc_tag.dir/_juha_swig_doc_tag.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib"
  "../include"
  "lib"
  "include"
  "/usr/local/include"
  "/usr/local/include/gnuradio/swig"
  "/usr/include/python2.7"
  "../swig"
  "swig"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
