
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") gr::juha::chirp_downconvert "<+description of block+>"

%feature("docstring") gr::juha::chirp_downconvert::make "Return a shared_ptr to a new instance of juha::chirp_downconvert.

To avoid accidental use of raw pointers, juha::chirp_downconvert's constructor is in a private implementation class. juha::chirp_downconvert::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::juha::chirp_downconvert::set_timing "

Params: (t, dur)"

%feature("docstring") gr::juha::chirp_downconvert::set_chirp_par "

Params: (f0, rate)"

%feature("docstring") gr::juha::chirp_downconvert::set_f0 "

Params: (f)"

%feature("docstring") gr::juha::chirp_downconvert::set_samp_rate "

Params: (r)"

%feature("docstring") gr::juha::chirp_downconvert::set_fname "

Params: (fname)"

%feature("docstring") gr::juha::chirp_downconvert::set_dec "

Params: (dec)"

%feature("docstring") gr::juha::chirp_downconvert::is_done "

Params: (NONE)"

%feature("docstring") gr::juha::chirp_downconvert::get_lag "

Params: (NONE)"

%feature("docstring") gr::juha::cspecsink "<+description of block+>"

%feature("docstring") gr::juha::cspecsink::make "Return a shared_ptr to a new instance of juha::cspecsink.

To avoid accidental use of raw pointers, juha::cspecsink's constructor is in a private implementation class. juha::cspecsink::make is the public interface for creating new instances.

Params: (fftlen, n_avg)"

%feature("docstring") gr::juha::filesink "<+description of block+>"

%feature("docstring") gr::juha::filesink::make "Return a shared_ptr to a new instance of juha::filesink.

To avoid accidental use of raw pointers, juha::filesink's constructor is in a private implementation class. juha::filesink::make is the public interface for creating new instances.

Params: (prefix, size, filesize)"

%feature("docstring") gr::juha::filesink::get_overflow "

Params: (NONE)"

%feature("docstring") gr::juha::specsink "<+description of block+>"

%feature("docstring") gr::juha::specsink::get_spec "

Params: (NONE)"

%feature("docstring") gr::juha::specsink::make "Return a shared_ptr to a new instance of juha::specsink.

To avoid accidental use of raw pointers, juha::specsink's constructor is in a private implementation class. juha::specsink::make is the public interface for creating new instances.

Params: (fftlen, n_avg)"

%feature("docstring") gr::juha::whiten "<+description of block+>"

%feature("docstring") gr::juha::whiten::make "Return a shared_ptr to a new instance of juha::whiten.

To avoid accidental use of raw pointers, juha::whiten's constructor is in a private implementation class. juha::whiten::make is the public interface for creating new instances.

Params: (nfft, navg)"