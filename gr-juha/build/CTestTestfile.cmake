# CMake generated Testfile for 
# Source directory: /home/allen/workarea/gnuradio/gr-juha
# Build directory: /home/allen/workarea/gnuradio/gr-juha/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(include/juha)
subdirs(lib)
subdirs(swig)
subdirs(python)
subdirs(grc)
subdirs(apps)
subdirs(docs)
