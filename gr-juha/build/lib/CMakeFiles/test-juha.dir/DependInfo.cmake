# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/allen/workarea/gnuradio/gr-juha/lib/qa_juha.cc" "/home/allen/workarea/gnuradio/gr-juha/build/lib/CMakeFiles/test-juha.dir/qa_juha.cc.o"
  "/home/allen/workarea/gnuradio/gr-juha/lib/test_juha.cc" "/home/allen/workarea/gnuradio/gr-juha/build/lib/CMakeFiles/test-juha.dir/test_juha.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib"
  "../include"
  "lib"
  "include"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/allen/workarea/gnuradio/gr-juha/build/lib/CMakeFiles/gnuradio-juha.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
