#  
# a list of chirp "carousels". each element in list is executed in a separate thread. 
# if each thread has more than one chirp sounder, the soundings are done in a round-robin fashion
# cylcing through each one. 
#
# rmin and rmax effect plotting only (and can be changed after the fact). 
# edit them so that the distance between you and the transmitter fits within
# rmin and rmax. 
sounders =[[
#{'rep':900,'chirpt':0,'name':'cyprus3','rate':0.1e6,'dur':250, 'cf':12.5e6, 'rmin':7500, 'rmax':17500,'fmin':0.0,'fmax':30.0},

{'rep':720,'chirpt':134,'name':'pr','rate':0.1e6,'dur':250, 'cf':12.5e6, 'rmin':2000, 'rmax':12000,'fmin':0.0,'fmax':30.0},
            {'rep':720,'chirpt':318,'name':'virginia','rate':0.1e6,'dur':250, 'cf':12.5e6, 'rmin':0, 'rmax':8000,'fmin':0.0,'fmax':30.0}]]

#[{'rep':720,'chirpt':96,'name':'Chirp-96','rate':0.05e6,'dur':600, 'cf':12.5e6,'rmin':15000, 'rmax':30000,'fmin':0.0,'fmax':30.0}],

#[{'rep':720,'chirpt':165,'name':'Chirp-165','rate':0.05e6,'dur':600, 'cf':12.5e6,'rmin':15000, 'rmax':30000,'fmin':0.0,'fmax':30.0}]]
            
#{'rep':300,'chirpt':137,'name':'longreach','rate':0.125e6,'dur':250, 'cf':12.5e6,'rmin':15000, 'rmax':30000,'fmin':0.0,'fmax':30.0}]]

#data_dir = "/data/chirp0/"
data_dir = "/home/allen/workarea/gnuradio/gr-juha/apps/chirpsounder/data1/ch0"

#sample_rate = 25e6
sample_rate = 25e6
#dec = 625
dec = 625
if_rate = sample_rate/dec
whiten = False
whiten_len = 8192
whiten_n = 30000

def get_all_sounders():
    sounder_list = []
    for sounder_thread in sounders:
        for sounder in sounder_thread:
            sounder_list.append(sounder)
    return(sounder_list)
