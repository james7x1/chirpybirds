#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: HF Transceiver
# Author: Allen Morrison
# Generated: Sat Jun 27 00:21:31 2020
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import math
import sip
import sys
import time


class HF_Transceiver(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "HF Transceiver")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("HF Transceiver")
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "HF_Transceiver")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.record = record = False
        self.freq = freq = 10.0e6
        self.filename = filename = "/dev/null"
        self.baseband_rate = baseband_rate = int(20e3)
        self.LO = LO = 10e6
        self.volume = volume = 0.1
        self.tr = tr = uhd.tune_request(freq,0,dsp_policy=uhd.tune_request.POLICY_MANUAL,dsp_freq=LO-freq,lo_freq_policy=uhd.tune_request.POLICY_MANUAL,lo_freq=LO)
        self.save_file = save_file = "/dev/null" if record == False else filename
        self.samp_rate = samp_rate = 500e3
        self.rit = rit = 0
        self.mode = mode = 0
        self.mic_sensitivity = mic_sensitivity = 0.1
        
        self.lsb_taps = lsb_taps = firdes.complex_band_pass(1.0, baseband_rate, 10, 2.5e3, 100, firdes.WIN_HAMMING, 6.76)
          
        self.gain = gain = 25.0
        self.PTT = PTT = True

        ##################################################
        # Blocks
        ##################################################
        self._volume_range = Range(0.0, 1.0, 0.025, 0.1, 200)
        self._volume_win = RangeWidget(self._volume_range, self.set_volume, "Volume", "counter_slider", float)
        self.top_grid_layout.addWidget(self._volume_win, 0,1,1,1)
        self._rit_range = Range(-300, 300, 5.0, 0, 200)
        self._rit_win = RangeWidget(self._rit_range, self.set_rit, "Fine Tune", "counter_slider", float)
        self.top_grid_layout.addWidget(self._rit_win, 1,0,1,2)
        self._mode_options = (0, 1, 2, 3, )
        self._mode_labels = ("AM(SYNC)", "USB", "LSB", "AM(ENV)", )
        self._mode_group_box = Qt.QGroupBox("Demod mode")
        self._mode_box = Qt.QHBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._mode_button_group = variable_chooser_button_group()
        self._mode_group_box.setLayout(self._mode_box)
        for i, label in enumerate(self._mode_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._mode_box.addWidget(radio_button)
        	self._mode_button_group.addButton(radio_button, i)
        self._mode_callback = lambda i: Qt.QMetaObject.invokeMethod(self._mode_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._mode_options.index(i)))
        self._mode_callback(self.mode)
        self._mode_button_group.buttonClicked[int].connect(
        	lambda i: self.set_mode(self._mode_options[i]))
        self.top_grid_layout.addWidget(self._mode_group_box, 1,2,1,1)
        self._mic_sensitivity_range = Range(0.0, 1.0, 0.025, 0.1, 200)
        self._mic_sensitivity_win = RangeWidget(self._mic_sensitivity_range, self.set_mic_sensitivity, "Mic Sensitivity", "counter_slider", float)
        self.top_layout.addWidget(self._mic_sensitivity_win)
        self._gain_range = Range(0.0, 50, 1.0, 25.0, 200)
        self._gain_win = RangeWidget(self._gain_range, self.set_gain, "RF Gain", "counter_slider", float)
        self.top_grid_layout.addWidget(self._gain_win, 0,2,1,1)
        self._freq_tool_bar = Qt.QToolBar(self)
        self._freq_tool_bar.addWidget(Qt.QLabel("Frequency"+": "))
        self._freq_line_edit = Qt.QLineEdit(str(self.freq))
        self._freq_tool_bar.addWidget(self._freq_line_edit)
        self._freq_line_edit.returnPressed.connect(
        	lambda: self.set_freq(eng_notation.str_to_num(str(self._freq_line_edit.text().toAscii()))))
        self.top_grid_layout.addWidget(self._freq_tool_bar, 0,0,1,1)
        _PTT_push_button = Qt.QPushButton("PTT")
        self._PTT_choices = {'Pressed': False, 'Released': True}
        _PTT_push_button.pressed.connect(lambda: self.set_PTT(self._PTT_choices['Pressed']))
        _PTT_push_button.released.connect(lambda: self.set_PTT(self._PTT_choices['Released']))
        self.top_grid_layout.addWidget(_PTT_push_button, 4,1)
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join(("addr=192.168.10.2", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_source_0.set_subdev_spec("A:0", 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        self.uhd_usrp_source_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_source_0.set_center_freq(tr, 0)
        self.uhd_usrp_source_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("addrs = 192.168.10.2", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_time_now(uhd.time_spec(time.time()), uhd.ALL_MBOARDS)
        self.uhd_usrp_sink_0.set_center_freq(tr, 0)
        self.uhd_usrp_sink_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0.set_antenna("TX/RX", 0)
        _record_check_box = Qt.QCheckBox("record")
        self._record_choices = {True: True, False: False}
        self._record_choices_inv = dict((v,k) for k,v in self._record_choices.iteritems())
        self._record_callback = lambda i: Qt.QMetaObject.invokeMethod(_record_check_box, "setChecked", Qt.Q_ARG("bool", self._record_choices_inv[i]))
        self._record_callback(self.record)
        _record_check_box.stateChanged.connect(lambda i: self.set_record(self._record_choices[bool(i)]))
        self.top_layout.addWidget(_record_check_box)
        self.qtgui_freq_sink_x_0_0 = qtgui.freq_sink_c(
        	2048, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	freq, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_0_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0_0.set_y_axis(-140, -20)
        self.qtgui_freq_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0_0.enable_grid(True)
        self.qtgui_freq_sink_x_0_0.set_fft_average(0.1)
        self.qtgui_freq_sink_x_0_0.enable_control_panel(False)
        
        if not True:
          self.qtgui_freq_sink_x_0_0.disable_legend()
        
        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_freq_sink_x_0_0.set_plot_pos_half(not True)
        
        labels = ["", "", "", "", "",
                  "", "", "", "", ""]
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0_0.set_line_alpha(i, alphas[i])
        
        self._qtgui_freq_sink_x_0_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_freq_sink_x_0_0_win)
        self.low_pass_filter_1_0 = filter.fir_filter_ccf(int((samp_rate/5)/baseband_rate), firdes.low_pass(
        	1, samp_rate/5, 2.2e3, 100, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_1 = filter.interp_fir_filter_ccf(5, firdes.low_pass(
        	1, samp_rate/5, 2.2e3, 100, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0_0 = filter.fir_filter_ccf(5, firdes.low_pass(
        	1, samp_rate, 20e3, 10e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0 = filter.interp_fir_filter_ccf(5, firdes.low_pass(
        	1, samp_rate, 20e3, 10e3, firdes.WIN_HAMMING, 6.76))
        self.freq_xlating_fir_filter_xxx_1_0 = filter.freq_xlating_fir_filter_ccc(1, ([1.0]), rit, baseband_rate)
        self.freq_xlating_fir_filter_xxx_1 = filter.freq_xlating_fir_filter_ccc(1, ([1.0]), rit, baseband_rate)
        self.freq_xlating_fir_filter_xxx_0_0 = filter.freq_xlating_fir_filter_ccc(1, (lsb_taps), -2.5e3, baseband_rate)
        self.freq_xlating_fir_filter_xxx_0 = filter.freq_xlating_fir_filter_ccc(1, (lsb_taps), -2.5e3, baseband_rate)
        self.fractional_resampler_xx_0_1 = filter.fractional_resampler_ff(0, baseband_rate/48e3)
        self.fractional_resampler_xx_0_0 = filter.fractional_resampler_ff(0, baseband_rate/8e3)
        self.fractional_resampler_xx_0 = filter.fractional_resampler_ff(0, baseband_rate*48e3)
        self._filename_tool_bar = Qt.QToolBar(self)
        self._filename_tool_bar.addWidget(Qt.QLabel("Recording file"+": "))
        self._filename_line_edit = Qt.QLineEdit(str(self.filename))
        self._filename_tool_bar.addWidget(self._filename_line_edit)
        self._filename_line_edit.returnPressed.connect(
        	lambda: self.set_filename(str(str(self._filename_line_edit.text().toAscii()))))
        self.top_layout.addWidget(self._filename_tool_bar)
        self.blocks_wavfile_sink_0 = blocks.wavfile_sink(save_file, 1, 8000, 8)
        self.blocks_multiply_xx_1 = blocks.multiply_vff(1)
        self.blocks_multiply_xx_0_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_vff((volume, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vff((mic_sensitivity, ))
        self.blocks_magphase_to_complex_0 = blocks.magphase_to_complex(1)
        self.blocks_float_to_complex_1_1 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_1 = blocks.float_to_complex(1)
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_complex_to_real_2 = blocks.complex_to_real(1)
        self.blocks_complex_to_real_0_0 = blocks.complex_to_real(1)
        self.blocks_complex_to_real_0 = blocks.complex_to_real(1)
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(1)
        self.blocks_add_xx_1_0 = blocks.add_vcc(1)
        self.blocks_add_xx_1 = blocks.add_vcc(1)
        self.blks2_valve_0_0 = grc_blks2.valve(item_size=gr.sizeof_gr_complex*1, open=bool(not PTT))
        self.blks2_valve_0 = grc_blks2.valve(item_size=gr.sizeof_float*1, open=bool(PTT))
        self.blks2_selector_0_1 = grc_blks2.selector(
        	item_size=gr.sizeof_gr_complex*1,
        	num_inputs=4,
        	num_outputs=1,
        	input_index=mode,
        	output_index=0,
        )
        self.blks2_selector_0_0 = grc_blks2.selector(
        	item_size=gr.sizeof_float*1,
        	num_inputs=4,
        	num_outputs=1,
        	input_index=mode,
        	output_index=0,
        )
        self.blks2_selector_0 = grc_blks2.selector(
        	item_size=gr.sizeof_float*1,
        	num_inputs=1,
        	num_outputs=4,
        	input_index=mode,
        	output_index=0,
        )
        self.band_pass_filter_2_1 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, baseband_rate, -3.2e3, -20, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2_0_0 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, baseband_rate, 20, 3.2e3, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2_0 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, baseband_rate, 20, 3.2e3, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, baseband_rate, -3.2e3, -20, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_1_0 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, baseband_rate, 10, 2.5e3, 100, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_1 = filter.fir_filter_ccc(1, firdes.complex_band_pass(
        	1, baseband_rate, 10, 2.5e3, 100, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_1 = filter.fir_filter_fff(1, firdes.band_pass(
        	1, baseband_rate, 20, baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_0_0 = filter.fir_filter_fff(1, firdes.band_pass(
        	1, baseband_rate, 20, baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_0 = filter.fir_filter_fff(1, firdes.band_pass(
        	1, baseband_rate, 20, baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0 = filter.fir_filter_fff(1, firdes.band_pass(
        	1, baseband_rate, 20, baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.audio_source_0 = audio.source(48000, "sysdefault", True)
        self.audio_sink_0 = audio.sink(48000, "sysdefault", True)
        self.analog_pll_refout_cc_0_0 = analog.pll_refout_cc(3.14159/200, (10*2*3.14159)/baseband_rate, (-10*2*3.14159)/baseband_rate)
        self.analog_pll_refout_cc_0 = analog.pll_refout_cc(3.14159/200, (10*2*3.14159)/baseband_rate, (-10*2*3.14159)/baseband_rate)
        self.analog_const_source_x_0 = analog.sig_source_f(0, analog.GR_CONST_WAVE, 0, 0, 0)
        self.analog_agc_xx_0_0 = analog.agc_cc(1e-3, 0.300, 1.0)
        self.analog_agc_xx_0_0.set_max_gain(1.0e10)
        self.analog_agc_xx_0 = analog.agc_cc(1e-3, 0.300, 1.0)
        self.analog_agc_xx_0.set_max_gain(1.0e10)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.qtgui_freq_sink_x_0_0, 'freq'), (self.qtgui_freq_sink_x_0_0, 'freq'))    
        self.msg_connect((self.qtgui_freq_sink_x_0_0, 'freq'), (self.uhd_usrp_source_0, 'command'))    
        self.connect((self.analog_agc_xx_0, 0), (self.low_pass_filter_1, 0))    
        self.connect((self.analog_agc_xx_0_0, 0), (self.freq_xlating_fir_filter_xxx_1_0, 0))    
        self.connect((self.analog_const_source_x_0, 0), (self.blocks_magphase_to_complex_0, 1))    
        self.connect((self.analog_pll_refout_cc_0, 0), (self.blocks_multiply_xx_0, 0))    
        self.connect((self.analog_pll_refout_cc_0_0, 0), (self.blocks_multiply_xx_0_0, 1))    
        self.connect((self.audio_source_0, 0), (self.blks2_valve_0, 0))    
        self.connect((self.band_pass_filter_0, 0), (self.blocks_float_to_complex_0, 0))    
        self.connect((self.band_pass_filter_0_0, 0), (self.blocks_multiply_xx_1, 0))    
        self.connect((self.band_pass_filter_0_0, 0), (self.blocks_multiply_xx_1, 1))    
        self.connect((self.band_pass_filter_0_0_0, 0), (self.blks2_selector_0_0, 3))    
        self.connect((self.band_pass_filter_0_1, 0), (self.blks2_selector_0_0, 0))    
        self.connect((self.band_pass_filter_1, 0), (self.blks2_selector_0_1, 1))    
        self.connect((self.band_pass_filter_1_0, 0), (self.blocks_complex_to_real_0, 0))    
        self.connect((self.band_pass_filter_2, 0), (self.blocks_add_xx_1, 1))    
        self.connect((self.band_pass_filter_2_0, 0), (self.blocks_add_xx_1, 0))    
        self.connect((self.band_pass_filter_2_0_0, 0), (self.blocks_add_xx_1_0, 1))    
        self.connect((self.band_pass_filter_2_1, 0), (self.blocks_add_xx_1_0, 0))    
        self.connect((self.blks2_selector_0, 0), (self.band_pass_filter_0, 0))    
        self.connect((self.blks2_selector_0, 3), (self.band_pass_filter_0_0, 0))    
        self.connect((self.blks2_selector_0, 1), (self.blocks_float_to_complex_1, 0))    
        self.connect((self.blks2_selector_0, 2), (self.blocks_float_to_complex_1_1, 0))    
        self.connect((self.blks2_selector_0_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))    
        self.connect((self.blks2_selector_0_1, 0), (self.freq_xlating_fir_filter_xxx_1, 0))    
        self.connect((self.blks2_valve_0, 0), (self.fractional_resampler_xx_0, 0))    
        self.connect((self.blks2_valve_0_0, 0), (self.low_pass_filter_0_0, 0))    
        self.connect((self.blks2_valve_0_0, 0), (self.qtgui_freq_sink_x_0_0, 0))    
        self.connect((self.blocks_add_xx_1, 0), (self.blocks_multiply_xx_0, 1))    
        self.connect((self.blocks_add_xx_1_0, 0), (self.blocks_multiply_xx_0_0, 0))    
        self.connect((self.blocks_complex_to_mag_squared_0, 0), (self.band_pass_filter_0_0_0, 0))    
        self.connect((self.blocks_complex_to_real_0, 0), (self.blks2_selector_0_0, 1))    
        self.connect((self.blocks_complex_to_real_0_0, 0), (self.blks2_selector_0_0, 2))    
        self.connect((self.blocks_complex_to_real_2, 0), (self.band_pass_filter_0_1, 0))    
        self.connect((self.blocks_float_to_complex_0, 0), (self.analog_pll_refout_cc_0, 0))    
        self.connect((self.blocks_float_to_complex_0, 0), (self.band_pass_filter_2, 0))    
        self.connect((self.blocks_float_to_complex_0, 0), (self.band_pass_filter_2_0, 0))    
        self.connect((self.blocks_float_to_complex_1, 0), (self.band_pass_filter_1, 0))    
        self.connect((self.blocks_float_to_complex_1_1, 0), (self.freq_xlating_fir_filter_xxx_0, 0))    
        self.connect((self.blocks_magphase_to_complex_0, 0), (self.blks2_selector_0_1, 3))    
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blks2_selector_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.fractional_resampler_xx_0_0, 0))    
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.fractional_resampler_xx_0_1, 0))    
        self.connect((self.blocks_multiply_xx_0, 0), (self.blks2_selector_0_1, 0))    
        self.connect((self.blocks_multiply_xx_0_0, 0), (self.blocks_complex_to_real_2, 0))    
        self.connect((self.blocks_multiply_xx_1, 0), (self.blocks_magphase_to_complex_0, 0))    
        self.connect((self.fractional_resampler_xx_0, 0), (self.blocks_multiply_const_vxx_0, 0))    
        self.connect((self.fractional_resampler_xx_0_0, 0), (self.blocks_wavfile_sink_0, 0))    
        self.connect((self.fractional_resampler_xx_0_1, 0), (self.audio_sink_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_0, 0), (self.blks2_selector_0_1, 2))    
        self.connect((self.freq_xlating_fir_filter_xxx_0_0, 0), (self.blocks_complex_to_real_0_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1, 0), (self.analog_agc_xx_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1_0, 0), (self.analog_pll_refout_cc_0_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1_0, 0), (self.band_pass_filter_1_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1_0, 0), (self.band_pass_filter_2_0_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1_0, 0), (self.band_pass_filter_2_1, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1_0, 0), (self.blocks_complex_to_mag_squared_0, 0))    
        self.connect((self.freq_xlating_fir_filter_xxx_1_0, 0), (self.freq_xlating_fir_filter_xxx_0_0, 0))    
        self.connect((self.low_pass_filter_0, 0), (self.uhd_usrp_sink_0, 0))    
        self.connect((self.low_pass_filter_0_0, 0), (self.low_pass_filter_1_0, 0))    
        self.connect((self.low_pass_filter_1, 0), (self.low_pass_filter_0, 0))    
        self.connect((self.low_pass_filter_1_0, 0), (self.analog_agc_xx_0_0, 0))    
        self.connect((self.uhd_usrp_source_0, 0), (self.blks2_valve_0_0, 0))    

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "HF_Transceiver")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()


    def get_record(self):
        return self.record

    def set_record(self, record):
        self.record = record
        self._record_callback(self.record)
        self.set_save_file("/dev/null" if self.record == False else self.filename)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        Qt.QMetaObject.invokeMethod(self._freq_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.freq)))
        self.set_tr(uhd.tune_request(self.freq,0,dsp_policy=uhd.tune_request.POLICY_MANUAL,dsp_freq=self.LO-self.freq,lo_freq_policy=uhd.tune_request.POLICY_MANUAL,lo_freq=self.LO))
        self.qtgui_freq_sink_x_0_0.set_frequency_range(self.freq, self.samp_rate)

    def get_filename(self):
        return self.filename

    def set_filename(self, filename):
        self.filename = filename
        Qt.QMetaObject.invokeMethod(self._filename_line_edit, "setText", Qt.Q_ARG("QString", str(self.filename)))
        self.set_save_file("/dev/null" if self.record == False else self.filename)

    def get_baseband_rate(self):
        return self.baseband_rate

    def set_baseband_rate(self, baseband_rate):
        self.baseband_rate = baseband_rate
        self.analog_pll_refout_cc_0.set_max_freq((10*2*3.14159)/self.baseband_rate)
        self.analog_pll_refout_cc_0.set_min_freq((-10*2*3.14159)/self.baseband_rate)
        self.analog_pll_refout_cc_0_0.set_max_freq((10*2*3.14159)/self.baseband_rate)
        self.analog_pll_refout_cc_0_0.set_min_freq((-10*2*3.14159)/self.baseband_rate)
        self.band_pass_filter_0.set_taps(firdes.band_pass(1, self.baseband_rate, 20, self.baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_0.set_taps(firdes.band_pass(1, self.baseband_rate, 20, self.baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_0_0.set_taps(firdes.band_pass(1, self.baseband_rate, 20, self.baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_0_1.set_taps(firdes.band_pass(1, self.baseband_rate, 20, self.baseband_rate/2.5, 60, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_1.set_taps(firdes.complex_band_pass(1, self.baseband_rate, 10, 2.5e3, 100, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_1_0.set_taps(firdes.complex_band_pass(1, self.baseband_rate, 10, 2.5e3, 100, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2.set_taps(firdes.complex_band_pass(1, self.baseband_rate, -3.2e3, -20, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2_0.set_taps(firdes.complex_band_pass(1, self.baseband_rate, 20, 3.2e3, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2_0_0.set_taps(firdes.complex_band_pass(1, self.baseband_rate, 20, 3.2e3, 15, firdes.WIN_HAMMING, 6.76))
        self.band_pass_filter_2_1.set_taps(firdes.complex_band_pass(1, self.baseband_rate, -3.2e3, -20, 15, firdes.WIN_HAMMING, 6.76))
        self.fractional_resampler_xx_0.set_resamp_ratio(self.baseband_rate*48e3)
        self.fractional_resampler_xx_0_0.set_resamp_ratio(self.baseband_rate/8e3)
        self.fractional_resampler_xx_0_1.set_resamp_ratio(self.baseband_rate/48e3)

    def get_LO(self):
        return self.LO

    def set_LO(self, LO):
        self.LO = LO
        self.set_tr(uhd.tune_request(self.freq,0,dsp_policy=uhd.tune_request.POLICY_MANUAL,dsp_freq=self.LO-self.freq,lo_freq_policy=uhd.tune_request.POLICY_MANUAL,lo_freq=self.LO))

    def get_volume(self):
        return self.volume

    def set_volume(self, volume):
        self.volume = volume
        self.blocks_multiply_const_vxx_0_0.set_k((self.volume, ))

    def get_tr(self):
        return self.tr

    def set_tr(self, tr):
        self.tr = tr
        self.uhd_usrp_sink_0.set_center_freq(self.tr, 0)
        self.uhd_usrp_source_0.set_center_freq(self.tr, 0)

    def get_save_file(self):
        return self.save_file

    def set_save_file(self, save_file):
        self.save_file = save_file
        self.blocks_wavfile_sink_0.open(self.save_file)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.low_pass_filter_0.set_taps(firdes.low_pass(1, self.samp_rate, 20e3, 10e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(1, self.samp_rate, 20e3, 10e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_1.set_taps(firdes.low_pass(1, self.samp_rate/5, 2.2e3, 100, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_1_0.set_taps(firdes.low_pass(1, self.samp_rate/5, 2.2e3, 100, firdes.WIN_HAMMING, 6.76))
        self.qtgui_freq_sink_x_0_0.set_frequency_range(self.freq, self.samp_rate)
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_rit(self):
        return self.rit

    def set_rit(self, rit):
        self.rit = rit
        self.freq_xlating_fir_filter_xxx_1.set_center_freq(self.rit)
        self.freq_xlating_fir_filter_xxx_1_0.set_center_freq(self.rit)

    def get_mode(self):
        return self.mode

    def set_mode(self, mode):
        self.mode = mode
        self._mode_callback(self.mode)
        self.blks2_selector_0.set_input_index(int(self.mode))
        self.blks2_selector_0_0.set_input_index(int(self.mode))
        self.blks2_selector_0_1.set_input_index(int(self.mode))

    def get_mic_sensitivity(self):
        return self.mic_sensitivity

    def set_mic_sensitivity(self, mic_sensitivity):
        self.mic_sensitivity = mic_sensitivity
        self.blocks_multiply_const_vxx_0.set_k((self.mic_sensitivity, ))

    def get_lsb_taps(self):
        return self.lsb_taps

    def set_lsb_taps(self, lsb_taps):
        self.lsb_taps = lsb_taps
        self.freq_xlating_fir_filter_xxx_0.set_taps((self.lsb_taps))
        self.freq_xlating_fir_filter_xxx_0_0.set_taps((self.lsb_taps))

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_0.set_gain(self.gain, 0)
        	
        self.uhd_usrp_source_0.set_gain(self.gain, 0)
        	

    def get_PTT(self):
        return self.PTT

    def set_PTT(self, PTT):
        self.PTT = PTT
        self.blks2_valve_0.set_open(bool(self.PTT))
        self.blks2_valve_0_0.set_open(bool(not self.PTT))


def main(top_block_cls=HF_Transceiver, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
